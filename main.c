#include <stdio.h>
#include <SMSlib.h>
#include "flash/flash.h" 

// Address which will be erased and written. Make sure there's no code there!
#define TEST_ADDR 0x20000

void main(void)
{
	// Clear VRAM
	SMS_VRAMmemsetW(0, 0x0000, 16384);

	// load a standard font character set into tiles 0-95,
	// set BG palette to B/W and turn on the screen
	SMS_autoSetUpTextRenderer();

	// Read and print flash IDs
	uint8_t ids[2];
	flash_ids_get(ids);
	SMS_setNextTileatXY(4, 10);
	printf("IDs: 0x%02X%02X", ids[0], ids[1]);

	// Erase a sector
	flash_sector_erase(TEST_ADDR);
	SMS_printatXY(4, 11,"Sector erased!");

	// Program a single byte
	flash_program_byte(TEST_ADDR + 0x10, 0x55);
	SMS_printatXY(4, 12,"Program byte done!");

	// Program an array of bytes
	static const uint8_t hello[] = "Hello world!";
	flash_program(TEST_ADDR, hello, sizeof(hello));
	SMS_printatXY(4, 13,"Program hello done!");

	// Turn on the display
	SMS_displayOn();

	// Do nothing
	for(;;);
}

SMS_EMBED_SEGA_ROM_HEADER(9999,0);
SMS_EMBED_SDSC_HEADER_AUTO_DATE(1,0,"doragasu","flash example","simple flash write code");
