# Introduction

This is a very simple example showing how to use [FrugalMap Save](https://gitlab.com/doragasu/frugal-map-save) to save data into flash based cartridges. The code performs the following actions:

* Read the flash IDs and print them on the screen.
* Erase one flash sector.
* Write one byte to flash.
* Write the "Hello world!" string to flash.

Make sure to read the [FrugalMap Save](https://gitlab.com/doragasu/frugal-map-save) documentation to understand how flash saving works.

# Building

The project uses [devkitSMS](https://github.com/sverx/devkitSMS), and has an automated CI/CD pipeline that builds the code using [a Docker image from retcon85](https://hub.docker.com/r/retcon85/toolchain-sms). You can have a look to the [Gitlab CI/CD file](.gitlab-ci.yml) file for more details about the build.

# Testing

To test if this code works with your cart, you can download a ROM from the [Package Registry](https://gitlab.com/doragasu/frugal-map-save-example/-/packages). You will have to burn it to your cart, run it in a console and then dump it and check if there is data written at address `0x20000`.

# Author

The code in this repository has been written by Jesús Alonso (doragasu). Merge requests are welcome!

# License

This code is provided with NO WARRANTY under the MIT license.
