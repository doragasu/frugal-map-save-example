CC=sdcc
IHX2SMS=ihx2sms
TOOLCHAIN_DIR=/usr/local/share/sdcc
SMSLIB_INCDIR=$(TOOLCHAIN_DIR)/include/sms
PEEP_RULES=$(TOOLCHAIN_DIR)/lib/sms/peep-rules.txt
CRT0=$(TOOLCHAIN_DIR)/lib/sms/crt0_sms.rel
SMSLIB_LIB=$(TOOLCHAIN_DIR)/lib/sms/SMSlib.lib

CFLAGS=-mz80 -I$(SMSLIB_INCDIR) --peep-file $(PEEP_RULES)
LDFLAGS=-mz80 --no-std-crt0 --data-loc 0xC000

PROGNAME=frugalmap_save

OBJS=main.rel flash.rel

all: $(PROGNAME).sms

flash.rel: flash/flash.c
	$(CC) $(CFLAGS) -c $<

%.rel: %.c
	$(CC) $(CFLAGS) -c $<

%.rel: %.c %.h
	$(CC) $(CFLAGS) -c $<

$(PROGNAME).ihx: $(OBJS)
	$(CC) -o $@ $(LDFLAGS) $(CRT0) $(SMSLIB_LIB) $^

$(PROGNAME).sms: $(PROGNAME).ihx
	$(IHX2SMS) $< $@

clean:
	rm -f *.rel *.ihx *.asm *.sym *.lst *.noi *.lk *.map *.sms *.gg

# When a linking error occurs, sdcc returns an error and make fails. However
# a .ihx output file still gets created. This leads to make thinking that the .ihx
# file is up to date the next time it runs... This forces linking to take place
# every time make is called.
.PHONY: $(PROGNAME).ihx
